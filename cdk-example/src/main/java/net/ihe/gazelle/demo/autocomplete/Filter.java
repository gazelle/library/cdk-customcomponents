package net.ihe.gazelle.demo.autocomplete;

import javax.faces.bean.ManagedBean;

@ManagedBean
public class Filter {

	public AutocompleteBean suggester;

	public AutocompleteBean getSuggester() {
		if (suggester==null)
			suggester=new AutocompleteBean();
		return suggester;
	}

	public void setSuggester(AutocompleteBean suggester) {
		this.suggester = suggester;
	}
	
	
	
	
}
