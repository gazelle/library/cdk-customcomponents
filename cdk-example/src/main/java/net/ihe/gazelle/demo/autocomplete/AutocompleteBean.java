package net.ihe.gazelle.demo.autocomplete;

import java.util.ArrayList;
import java.util.List;





public class AutocompleteBean {

	private List<String> list;

	public List<String> getList() {
		if (list==null)
		{
			list=new ArrayList<String>();
			list.add("aaaaaa");
			list.add("abaaaa");
			list.add("abbbbb");
			list.add("baaaaa");
			list.add("bbbbbb");
			list.add("cccccc");
		}
		return list;
	}

	public void setList(List<String> list) {
		this.list = list;
	}

	public List<String> autocomplete(String prefix) {
		List<String> allValues = (List<String>) getList();
		List<String> values = allValues;

		// show all items if something is already selected and the label match
		// the selected item
		boolean showAllItems = true;

		if ((allValues != null) && (prefix != null) && (prefix.length() > 1)) {
			showAllItems = false;
		}

		if (!showAllItems) {
			prefix = prefix.toLowerCase();
			values = new ArrayList<String>();
			for (String e : allValues) {
				String label =  e;
				if (label != null) {
					label = label.toLowerCase();
					if (label.contains(prefix)) {
						values.add(e);
					}
				}
			}
		}
		return values;
	}
}
