package net.ihe.gazelle.demo.column.DataModel;


 
import java.util.ArrayList;
import java.util.List;

import javax.faces.context.FacesContext;

 

import org.ajax4jsf.model.DataVisitor;
import org.ajax4jsf.model.ExtendedDataModel;
import org.ajax4jsf.model.Range;
import org.ajax4jsf.model.SequenceRange;
import org.richfaces.component.SortOrder;
import org.richfaces.model.Arrangeable;
import org.richfaces.model.ArrangeableState;
import org.richfaces.model.FilterField;
import org.richfaces.model.SortField;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
 
public abstract class JPADataModel<T> extends ExtendedDataModel<T> implements Arrangeable {
  
    private Object rowKey;
    private ArrangeableState arrangeableState;
    private Class<T> entityClass;
    public List<T> list=new ArrayList<T>();
    public JPADataModel( Class<T> entityClass) {
        super();
 
        this.entityClass = entityClass;
    }
 
    public void arrange(FacesContext context, ArrangeableState state) {
        arrangeableState = state;
    }
 
    @Override
    public void setRowKey(Object key) {
        rowKey = key;
    }
 
    @Override
    public Object getRowKey() {
        return rowKey;
    }
 


 

 
    protected ArrangeableState getArrangeableState() {
        return arrangeableState;
    }
 
    protected Class<T> getEntityClass() {
        return entityClass;
    }
 

 
   
 
    @Override
    public void walk(FacesContext context, DataVisitor visitor, Range range, Object argument) {
    
    	
 
        
 
        List<T> data = list;
        for (T t : data) {
            visitor.process(context, getId(t), argument);
        }
    }
 
    @Override
    public boolean isRowAvailable() {
        return rowKey != null;
    }
 
    @Override
    public int getRowCount() {
    	return list.size();
    }
 
    @Override
    public T getRowData() {
        return list.get((Integer)rowKey);
    }
 
    @Override
    public int getRowIndex() {
        return -1;
    }
 
    @Override
    public void setRowIndex(int rowIndex) {
        throw new UnsupportedOperationException();
    }
 
    @Override
    public Object getWrappedData() {
        throw new UnsupportedOperationException();
    }
 
    @Override
    public void setWrappedData(Object data) {
        throw new UnsupportedOperationException();
    }
 
    // TODO - implement using metadata
    protected abstract Object getId(T t);
}