package net.ihe.gazelle.tags.component;



import org.richfaces.cdk.annotations.Attribute;
import org.richfaces.cdk.annotations.JsfComponent;
import org.richfaces.cdk.annotations.JsfRenderer;
import org.richfaces.cdk.annotations.Tag;
import org.richfaces.component.UIColumn;

@JsfComponent(family = AbstractGazelleColumn.COMPONENT_FAMILY, type = AbstractGazelleColumn.COMPONENT_TYPE, tag = @Tag(name = AbstractGazelleColumn.TAG_NAME), renderer = @JsfRenderer(type = "net.ihe.gazelle.column"))
public abstract   class AbstractGazelleColumn  extends UIColumn{
	public static final String COMPONENT_TYPE = "net.ihe.gazelle.Column";
	public static final String COMPONENT_FAMILY = "net.ihe.gazelle.Column";
	public static final String TAG_NAME = "column";
	
	public final static String FilterEvent_KeyUp = "keyup";
	public final static String FilterEvent_change = "change";

	
	
	@Attribute(defaultValue=FilterEvent_change,suggestedValue = FilterEvent_KeyUp + "," + FilterEvent_change )
		    public abstract String getFilterEvent();
	

}
